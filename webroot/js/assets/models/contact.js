App.Models.Contact = Backbone.Model.extend({
	idAttribute : 'id',
	urlRoot : 'contacts',
	defaults : {
		photo : "img/default.gif",
		name : '',
		type : '',
		tel : '',
		email : '',
		address : ''
	}
});