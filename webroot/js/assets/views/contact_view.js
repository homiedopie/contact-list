App.Views.ContactView = Backbone.View.extend({
	template : _.template($('#contact-item-template').html()),
	editTemplate : _.template($('#contact-edit-template').html()),
	events : {
		'click button.contact-delete' : 'deleteContact',
		'click button.contact-edit' : 'editContact',
		'click button.contact-save' : 'updateContact',
		'click button.contact-save-cancel' : 'cancelContact'
	},
	initialize : function(){
		console.log('set bind');
		this.model.on('change', this.render, this);
	},
	render : function(){
		//this.el = this.template(this.model.toJSON());
		//this.delegateEvents(this.events);
		console.log('redisplay');
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},
	deleteContact : function(e) {
		e.preventDefault();
		console.log('Trigger: deleteContact');
		var that = this;
		this.model.destroy({
			success : function(response){
				console.log('Handle: deleteContact');
				that.remove();
			},
			error : function(response) {
				console.log('error delete');
			}
		});
	},
	editContact : function(e) {
		this.$el.html(this.editTemplate(this.model.toJSON()));
		this.selectTypeView = new App.Views.SelectTypeView({
			collection : this.model.collection,
			selector : 'contact-type',
			selectorType : 'class',
			element : this.$el,
			value : this.model.get('type')
		});
		this.selectTypeView.updateSelect();
	},
	cancelContact : function(e) {
		this.render();
	},
	updateContact : function(e){
		var that = this;
		this.model.save(this.parseInput(),{
			success : function(response) {
				response.trigger('change');
			},
			error : function(response) {
				console.log("Error Updating");
			},
			wait : true,
			silent : true
		});
	},
	parseInput : function() {
		return {
			id : this.$el.find('.contact-id').val(),
			name : this.$el.find('.contact-name').val(),
			photo : 'img/default.gif',
			type : this.$el.find('.contact-type').val(),
			tel : this.$el.find('.contact-telephone').val(),
			email : this.$el.find('.contact-email').val(),
			address : this.$el.find('.contact-address').val(),
		};
	}
});