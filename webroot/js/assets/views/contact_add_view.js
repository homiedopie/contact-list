App.Views.ContactAddView = Backbone.View.extend({
	id : 'contact-add-form',
	template : _.template($('#contact-add-form-template').html()),
	attributes : {
		'style' : 'display: none;' 
	},
	//$addForm : $(this.addForm),
	events : {
		'click #contact-add-save' : 'addContact',
		'click #contact-add-cancel' : 'cancelContact',
	},
	initialize : function () {
		this.render();
		this.listenTo(App.Events.Dispatcher, 'sendList', this.getList);
		this.listenTo(App.Events.Dispatcher, 'sendTypeList', this.getTypeList);
	},
	render : function(callback) {
		this.$el.html(this.template());
		return this;
	},
	addContact : function(e) {
		App.Events.Dispatcher.trigger('addRecord');
		if(this.collection){
	       // if(this.model.isNew()) {
	       	var that = this;
	            this.collection.create(this.parseInput(), {
	            	success : function(response){
	            		that.$el.slideUp();
	            		that.remove();
	            		$('html, body').animate({ scrollTop: $('#head_'+response.id).offset().top }, 'slow');
	            	}, 
	            	error : function(response) {
	            		console.log('test add contact error');
	            	}, 
	            	wait : true
	            });
	       // }
		}
		e.preventDefault();
	},
	cancelContact : function() {
		this.$el.slideUp();
		this.remove();
	},
	parseInput : function() {
		return {
			id : this.$el.find('#contact-id').val(),
			name : this.$el.find('#contact-name').val(),
			photo : 'img/default.gif',
			type : this.$el.find('#contact-type').val(),
			tel : this.$el.find('#contact-telephone').val(),
			email : this.$el.find('#contact-email').val(),
			address : this.$el.find('#contact-address').val(),
		};
	},
	getList : function(collection) {
		console.log('Handle: sendList');
		this.collection = collection;
	},
	getTypeList : function(typeCollection) {
		console.log('Handle: sendTypeList');
		this.typeList = typeCollection;
		//this.updateSelect();
		this.selectTypeView = new App.Views.SelectTypeView({
			list : typeCollection,
			selector : 'contact-type',
			selectorType : 'id',
			element : this.$el
		});
		this.selectTypeView.updateSelect();
	}
});