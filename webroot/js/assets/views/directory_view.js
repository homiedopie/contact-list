App.Views.DirectoryView = Backbone.View.extend({
	el : '#contact-directory-list',
	//$addForm : $(this.addForm),
	initialize : function () {
		this.collection.bind("reset", this.render, this);
		this.collection.bind("add", this.renderItemView, this);
		this.listenTo(App.Events.Dispatcher, 'addRecord', this.returnCollection);
		this.listenTo(App.Events.Dispatcher, 'getTypeList', this.returnTypeCollection);
	},
	render : function(callback){
		var counter = 0;
		this.collection.each(function (contact) {
			counter++;
            this.$el.append(this.renderItemObject(contact));
            if(counter % 3 == 0) {
            	this.$el.append('<div class="clearfix visible-lg visible-md"></div>');
            }
            if(counter % 2 == 0) {
            	this.$el.append('<div class="clearfix visible-sm"></div>');
            }
        }, this);
        return this;
	},
	renderItemObject : function(model) {
		return new App.Views.ContactView({model: model}).render().el;
	},
	renderItemView : function(model){
		this.$el.append(this.renderItemObject(model));
		$('#head_'+model.id).find('h4').append('<span class="label label-success pull-right">New</span>').find('span.label').fadeOut(2000);
	},
	returnCollection : function(e){
		console.log('Trigger: sendList');
		App.Events.Dispatcher.trigger('sendList', this.collection);
	},
	returnTypeCollection : function(e){
		console.log('Trigger: sendTypeList');
		App.Events.Dispatcher.trigger('sendTypeList', this.getAllTypes());
	},
	getTypes : function(collection) {
		if(typeof collection !== "undefined")
		{
			return _.uniq(collection.pluck('type'), false, function(type){
				return type.toLowerCase();
			})
		} else { 
			return _.uniq(this.collection.pluck('type'), false, function(type){
				return type.toLowerCase();
			})
		}
	},
	getAllTypes : function(newList) {
		var contacts;
		if(newList){
			contacts = new Contacts();
		}
		return newList ? this.getTypes(fullDirectory) : this.getTypes();
	}
});