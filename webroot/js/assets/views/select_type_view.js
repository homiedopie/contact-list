App.Views.SelectTypeView = Backbone.View.extend({

	initialize : function(options) {
		this.filter = options.filter ? options.filter : false ;
		this.selectedValue = options.value ? options.value : false ;
		this.selector	= options.selector ? options.selector : false ;
		this.selectorType = options.selectorType ? options.selectorType : false ;
		this.$el =  options.element;
		this.collection = options.collection;
		this.list = options.list;

		var events = {};
		events['change select'+this.getFormattedSelector()] = 'addType';
		this.delegateEvents(events);
	},
	render : function() {

	},
	getTypesFromCollection : function(collection) {
		if(typeof collection !== "undefined")
		{
			return _.uniq(collection.pluck('type'), false, function(type){
				return type.toLowerCase();
			})
		} else { 
			return _.uniq(this.collection.pluck('type'), false, function(type){
				return type.toLowerCase();
			})
		}
	},
	getAllTypes : function(newList) {
		if(typeof this.list !== 'undefined')
		{
			return this.list;
		} else if (typeof this.collection !== 'undefined') {
			return this.getTypesFromCollection();
		} else {
			return [];
		}
	},
	createSelect : function() {
		var options = {
			'class' : 'form-control'
		};
		this.getFormOptions(options);
		var select = $('<select/>', options);
		_.each(this.getAllTypes(), function(item) {
			var option = $('<option/>', {
				value : item.toLowerCase(),
				text : item.toLowerCase()			
			}).appendTo(select);
		});
		var newOpt = $('<option/>', {
			html: '<em>Add new</em>',
			value: 'addType'
		});
		$(select).append(newOpt);
		if(this.selectedValue.length > 0){
			$(select).val(this.selectedValue);
		}
		return select;
	},
	updateSelect : function() {
		this.$el
		.find(this.getFormattedSelector())
		.replaceWith(this.createSelect());
		//Check selected value for prepopulate checking
		this.checkSelectedValue();
	},
	addType : function(evt) {
		var target = $(evt.target);
		var options = {
			'class' : 'form-control'
		};
		this.getFormOptions(options);
		var input_type = $('<input/>', options);

		if(target.val() === 'addType')
		{
			target.replaceWith(input_type);
		}
	},
	getFormattedSelector : function() {
		return this.selectorType === 'class' ? '.'+this.selector : '#'+this.selector;
	},
	getFormOptions : function(options) {
		var selectorType = this.selectorType;
		var selector = this.selector;
		if(selectorType === 'class'){
			options[selectorType] += ' '+selector;
		} else {
			options[selectorType] = selector;
		}
	},
	checkSelectedValue : function() {
		this.addType({target : this.getFormattedSelector()});
	}
});