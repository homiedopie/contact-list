App.Routers.AppRouter = Backbone.Router.extend({
	routes : {
		"" : "contactList",
		"filter/:type" : "contactFilter" 
	},
	initialize : function(){
		
	},
	contactList :  function(){
		console.log('run contact list view');
		this.contactList = new App.Collections.Contacts();
		this.contactList.fetch({ reset : true });
		this.directoryView = new App.Views.DirectoryView({
			collection : this.contactList
		});
		$('#contact-directory').append(this.directoryView.render().el);
	}
});