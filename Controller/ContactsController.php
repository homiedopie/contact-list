<?php

class ContactsController extends AppController 
{
    public function beforeFilter() 
    {
        if($this->action != 'home')
        {
            $this->autoRender = false;
            $this->response->type('json');
            $this->response->header('Access-Control-Allow-Methods', 'PUT, DELETE, POST, GET');
            $this->response->header('Access-Control-Allow-Headers', 'accept, content-type');
            $this->response->header('Access-Control-Allow-Origin', '*');  
        }
        
        $this->fields = $this->_getFields();

    }
    public function _getFields()
    {
        $fields = array_keys($this->Contact->getColumnTypes());
        $fields = array_flip($fields);

        unset($fields['modified']);
        unset($fields['created']);

        $fields = array_flip($fields);

        return $fields;
    }
    public function index()
    {
        //Added to avoid pre-flight data retrieval
        if($this->request->is('get')){

            $result_array = $this->Contact->find('all', array('fields' => $this->fields));
            $this->_trimContact($result_array);
            return json_encode($result_array);
        }

    }
    public function add()
    { 
        $this->Contact->create();
        if($this->Contact->save($this->request->input('json_decode'))){
            return json_encode(
                array(
                    'id' => $this->Contact->id
                )
            );
        }
        return json_encode(
            array(
                'result' => '0',
                'data' => $this->request->input('json_decode')
            )
        );
    	
    }
    public function _trimContact(&$result_array)
    {   
        foreach ($result_array as &$v){
           $this->_trimSingleContact($v);
        }
    }
    public function _trimSingleContact(&$single)
    {
        $single = $single['Contact'];
    }

    public function edit($id)
    {
        if(!$id){
            return json_encode(array('result' => 0, 'message' => 'No Id Found'));
        }
        $post = $this->Contact->findById($id);
        if(!$post){
            return json_encode(array('result' => 0, 'message' => 'No Post Found for Id'));
        }
        $this->Contact->id = $id;
    	if($this->Contact->save($this->request->input('json_decode'))){
            $result_array = $this->Contact->findById($id, $this->fields);
            $this->_trimSingleContact($result_array);
            return json_encode(
                $result_array
            );
        }
        return json_encode(
            array(
                'result' => '0',
                'data' => $this->request->input('json_decode')
            )
        );
    }
    public function delete($id)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Contact->delete($id)) {
            return json_encode(array('result' => 1));
        }
        return json_encode(array('result' => 0));
    }
    public function view($id)
    {
        if(!$id){
            return json_encode(array('result' => 0, 'message' => 'No Id Found'));
        }
        $post = $this->Post->findById($id);
        if(!$post){
            return json_encode(array('result' => 0, 'message' => 'No Post Found for Id'));
        }
        return json_encode($post);
    }
    public function upload(){
        return 'test';
    }
    public function options()
    {
        return 'test options';
    }
    public function options_id($id)
    {
        return 'test options'. $id;
    }
    public function home()
    {
        //$this->autoRender = false;
        //echo 'test-home';
    }
}