<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			            <span class="sr-only">Toggle navigation</span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			        </button>
			        <a class="navbar-brand" href="#">
			        	<span>Contact List</span>
			        </a>
				</div>
		        <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
		        	<!--
		          <form class="navbar-form navbar-right">
		            <div class="form-group">
		              <input type="text" placeholder="Email" class="form-control">
		            </div>
		            <div class="form-group">
		              <input type="password" placeholder="Password" class="form-control">
		            </div>
		            <button type="submit" class="btn btn-success">Sign in</button>
		          </form>
		      -->	
		      		<div class="directory-function">
		      			<button id="contact-open-form" class="btn btn-success">Add Contact</button>
		      		</div>
		      		
		        </div>
			</div>
		</nav>